package com.example.appium;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.html5.Location;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;

public class MobileExercise {
	AndroidDriver driver;

	@SuppressWarnings("unchecked")
	@BeforeClass
	public void beforeClass() throws MalformedURLException {
		DesiredCapabilities myCaps = new DesiredCapabilities();
		File app = new File("/Users/ferdinando.altamiran/Desktop/Appium/app-debug.apk");
		myCaps.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
		myCaps.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
		myCaps.setCapability(MobileCapabilityType.PLATFORM_VERSION, "9");
		myCaps.setCapability(MobileCapabilityType.DEVICE_NAME,"Emulator");
		myCaps.setCapability(MobileCapabilityType.AUTOMATION_NAME ,"UiAutomator2");
		myCaps.setCapability(MobileCapabilityType.SUPPORTS_LOCATION_CONTEXT ,true);		
		myCaps.setCapability(AndroidMobileCapabilityType.AVD,"Pixel_2_API_28");
		myCaps.setCapability(AndroidMobileCapabilityType.APP_PACKAGE ,"com.inmarket.testapp");
		myCaps.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY ,"com.inmarket.sampleapp.NavDrawerMainActivity");
		
		driver = new AndroidDriver(new URL("http://localhost:4723/wd/hub"), myCaps);
		driver.setLocation(new Location(34.007057, -118.393203, 6.0));
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);		
	}

	@Test
	public void testLocation() throws InterruptedException {		
		driver.findElement(By.id("com.inmarket.testapp:id/permissionsButton")).click();
		driver.findElement(By.id("com.android.packageinstaller:id/permission_allow_button")).click();
		
		WebElement testMode_Toogle = driver.findElement(By.id("com.inmarket.testapp:id/testMode"));
		if (testMode_Toogle.getAttribute("checked").equals("false")) {
			testMode_Toogle.click();	
		}
		
		WebElement stopFlag_Toogle = driver.findElement(By.id("com.inmarket.testapp:id/stopFlag"));
		if (stopFlag_Toogle.getAttribute("checked").equals("true")) {
			stopFlag_Toogle.click();	
		}

		WebElement geoFencing_Toogle = driver.findElement(By.id("com.inmarket.testapp:id/geofencing"));
		if (geoFencing_Toogle.getAttribute("checked").equals("false")) {
			geoFencing_Toogle.click();	
		}

//		NOTE: this toogle cannot be set to TRUE then verification is commented
//		Assert.assertTrue(testMode_Toogle.getAttribute("checked").equals("true"),"testMode value incorrect:");
		Assert.assertTrue(stopFlag_Toogle.getAttribute("checked").equals("false"),"stopFlag value incorrect:");
		Assert.assertTrue(geoFencing_Toogle.getAttribute("checked").equals("true"),"geoFencing value incorrect:");
		
		driver.findElement(By.className("android.widget.ImageButton")).click();
		
		List<WebElement> menuOptions = driver.findElements(By.id("com.inmarket.testapp:id/design_menu_item_text"));
		
		for (WebElement menuOpt:menuOptions) {
			if (menuOpt.getText().equals("Manual Checkin")) {
				menuOpt.click();
				break;
			}
		}
		
		List<WebElement> listOptions = driver.findElements(By.id("com.inmarket.testapp:id/list_item_name"));
		for (WebElement listOpt:listOptions) {
			if (listOpt.getText().equals("Ralphs")) {
				listOpt.click();
				break;
			}
		}
		Thread.sleep(5000);
		boolean ralphViewDisplayed = false;
		List<WebElement> ralphViews = driver.findElements(By.className("android.view.View"));
		for(WebElement curr_view:ralphViews) {
			if (curr_view.getText().equals("Ralphs")) {
				ralphViewDisplayed=true;
				break;
			}			
		}
		Assert.assertTrue(ralphViewDisplayed,"Ralphs view not displayed");
		
		driver.findElement(By.className("android.widget.Button")).click();		
		
//		NOTE: This code is to apply a swipe action instead of clicking on 'Open navigation drawer' burguer button
//		int startX = driver.manage().window().getSize().getWidth()-100;
//		int endX = driver.manage().window().getSize().getWidth()/9;
//		int startY = driver.manage().window().getSize().getHeight()/2;
//				
//		TouchAction swipeTouch = new TouchAction(driver);
//		swipeTouch.press(point(startX,startY)).waitAction(waitOptions(ofMillis(1000))).moveTo(point(endX,startY)).release();
//		swipeTouch.perform();
		Thread.sleep(5000);

	}

	@AfterClass
	public void afterClass() {
		driver.closeApp();
	}

}
