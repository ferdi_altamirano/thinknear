### About the implementation
The code is intended to run only for:
- Android platform
- AVD emulator

If it is necessary to run on a real device, only the below capabilities should be set in *MobileExercise.java* file:

		caps.setCapability(MobileCapabilityType.DEVICE_NAME,"<real_device_name>");
		caps.setCapability("UDID", "<real_device_id>");

And comment the following capabilities:

		myCaps.setCapability(AndroidMobileCapabilityType.AVD,"Pixel_2_API_28");

Also note that APK is set to be found in local directory:
		/Users/ferdinando.altamiran/Desktop/Appium/
		
### TODOs

- **Add Support for iOS devices** This version only supports Android devices working on an emulator.
- **Insert a logger manager** An specialized tool for login should be added to them implementation e.g. `log4j`
- **POM** Since the test application is navigating over many screens is recommendable to apply `Page Object Model` in order to obtain a well structured project.
- **Logic layering** In the same idea of having a project structure a differenciation between logic could be applied:
	- Source Code files
	- Constants files
	- Locators files
	- Common Utilities, for example a customizable WebDriver connection class.